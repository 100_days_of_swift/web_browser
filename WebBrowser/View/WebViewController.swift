//
//  WebViewController.swift
//  WebBrowser
//
//  Created by Hariharan S on 05/05/24.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    // MARK: - Property
    
    var urlString: String = ""
    private var webView: WKWebView!
    private var progressView: UIProgressView!
    private var backButton: UIBarButtonItem!
    private var forwardButton: UIBarButtonItem!
    
    override func loadView() {
        self.webView = WKWebView()
        self.webView.navigationDelegate = self
        self.view = self.webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBar()
        self.configureWebView(with: self.urlString)
    }
    
    override func observeValue(
        forKeyPath keyPath: String?,
        of object: Any?,
        change: [NSKeyValueChangeKey : Any]?,
        context: UnsafeMutableRawPointer?
    ) {
        guard keyPath == "estimatedProgress"
        else {
            return
        }
        self.progressView.progress = Float(webView.estimatedProgress)
    }
}

// MARK: - Private Methods

private extension WebViewController {
    func configureNavBar() {
        self.navigationItem.largeTitleDisplayMode = .never
        self.progressView = UIProgressView(progressViewStyle: .default)
        self.progressView.sizeToFit()
        
        self.backButton = UIBarButtonItem(
            image: UIImage(systemName: "chevron.backward"),
            style: .done,
            target: self.webView,
            action: #selector(self.webView.goBack)
        )
        
        self.forwardButton = UIBarButtonItem(
            image: UIImage(systemName: "chevron.forward"),
            style: .done,
            target: self.webView,
            action: #selector(self.webView.goForward)
        )
        
        let progressButton = UIBarButtonItem(customView: self.progressView)
        
        let spacer = UIBarButtonItem(
            barButtonSystemItem: .flexibleSpace,
            target: nil,
            action: nil
        )
        
        let refresh = UIBarButtonItem(
            barButtonSystemItem: .refresh,
            target: self.webView,
            action: #selector(self.webView.reload)
        )

        self.toolbarItems = [self.backButton, self.forwardButton, spacer, progressButton, spacer, refresh]
        self.navigationController?.isToolbarHidden = false
    }
    
    func configureWebView(with urlLink: String) {
        guard let url = URL(string: urlLink)
        else {
            self.showFailedAlert()
            return
        }
        
        self.webView.load(URLRequest(url: url))
        self.webView.allowsBackForwardNavigationGestures = true
        self.webView.addObserver(
            self,
            forKeyPath: #keyPath(WKWebView.estimatedProgress), 
            options: .new,
            context: nil
        )
    }
    
    func showFailedAlert() {
        let failedAlertVC = UIAlertController(
            title: "Oops!, Failed",
            message: "Something went wrong, Not able to Host the URL",
            preferredStyle: .alert
        )
        let dismissAction = UIAlertAction(
            title: "OK",
            style: .default
        ) { _ in
            self.navigationController?.popViewController(animated: true)
        }
        failedAlertVC.addAction(dismissAction)
        self.present(failedAlertVC, animated: true)
    }
}
// MARK: - WKNavigationDelegate Conformance

extension WebViewController: WKNavigationDelegate {
    func webView(
        _ webView: WKWebView,
        didFinish navigation: WKNavigation!
    ) {
        self.title = webView.title
    }
    
    func webView(
        _ webView: WKWebView,
        didCommit navigation: WKNavigation!
    ) {
        self.backButton.isEnabled = webView.canGoBack
        self.forwardButton.isEnabled = webView.canGoForward
    }
}
