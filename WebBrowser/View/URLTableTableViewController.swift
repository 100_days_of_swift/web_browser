//
//  URLTableTableViewController.swift
//  WebBrowser
//
//  Created by Hariharan S on 08/05/24.
//

import UIKit

struct WebsiteData {
    let name: String
    let link: String
}

class URLTableTableViewController: UITableViewController {

    // MARK: - Properties

    private var website: [WebsiteData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setWebsiteData()
        self.title = "List of Websites"
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
}

// MARK: - Private Method

private extension URLTableTableViewController {
    func setWebsiteData() {
        self.website.append(
            contentsOf: [
                WebsiteData(name: "Apple", link: "https://www.apple.com"),
                WebsiteData(name: "Amazon", link: "https://www.amazon.in"),
                WebsiteData(name: "Canva", link: "https://www.canva.com/en_gb"),
                WebsiteData(name: "Facebook", link: "https://www.facebook.com"),
                WebsiteData(name: "Flipkart", link: "https://www.flipkart.com"),
                WebsiteData(name: "Google", link: "https://www.google.com"),
                WebsiteData(name: "ICC", link: "https://www.icc-cricket.com"),
                WebsiteData(name: "IPL", link: "https://www.iplt20.com"),
                WebsiteData(name: "LinkedIn", link: "https://www.linkedin.com"),
                WebsiteData(name: "Restricted", link: ""),
                WebsiteData(name: "Stack Overflow", link: "https://stackoverflow.com")
            ]
        )
    }
}

// MARK: - UITableViewDataSource Conformance

extension URLTableTableViewController {
    override func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        self.website.count
    }
    
    override func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let urlCell = tableView.dequeueReusableCell(
            withIdentifier: "URLCell",
            for: indexPath
        )
        urlCell.textLabel?.text = self.website[indexPath.row].name
        return urlCell
    }
}

// MARK: - UITableViewDelegate Conformance

extension URLTableTableViewController {
    override func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        guard let webViewVC = self.storyboard?.instantiateViewController(identifier: "WebViewController") as? WebViewController 
        else {
            return
        }
        webViewVC.urlString = self.website[indexPath.row].link
        self.navigationController?.pushViewController(webViewVC, animated: true)
    }
}
